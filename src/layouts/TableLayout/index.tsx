import './styles.scss';
import { useParams } from 'react-router-dom';
import GridList from 'src/components/GridList';

const TableLayout = (): JSX.Element => {
  const { id } = useParams<{ id?: string }>();
  const tableId = id ? parseInt(id) : 0;

  return (
    <div id="page-content">
      <div style={{ overflow: 'visible', display: 'block' }}>
        <div id="game-area">
          <div id="board">
            <div id="grid-container">
              <GridList tableId={tableId} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableLayout;
