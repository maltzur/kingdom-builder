/* eslint-disable react/jsx-props-no-spreading */
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
} from '@chakra-ui/react';
import { invoke } from '@tauri-apps/api';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import SettlementRadio from './SettlementRadio';

export type FormData = {
  numPlayers: string;
  color: string;
};

const NewGameModal = ({ isOpen, onClose }: {
  isOpen: boolean;
  onClose: () => void;
}): JSX.Element => {
  const history = useHistory();

  const {
    register,
    handleSubmit,
    control,
    formState: { errors, isSubmitting },
  } = useForm<FormData>();

  const onSubmit = handleSubmit(
    (data) => {
      const args = {
        numPlayers: parseInt(data.numPlayers),
        color: parseInt(data.color),
      };
      invoke<number>('new_game', args).then((message) => {
        history.push(`/table/${message}`);
      });
    },
  );

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <form onSubmit={onSubmit}>
          <ModalHeader>Create New Game</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Number of players</FormLabel>
              <NumberInput allowMouseWheel defaultValue={2} min={2} max={5}>
                <NumberInputField
                  {...register('numPlayers')}
                />
                <NumberInputStepper>
                  <NumberIncrementStepper />
                  <NumberDecrementStepper />
                </NumberInputStepper>
              </NumberInput>
            </FormControl>
            <FormControl isInvalid={!!errors.color}>
              <FormLabel>Color</FormLabel>
              <SettlementRadio control={control} />
              <FormErrorMessage>
                {errors.color?.message}
              </FormErrorMessage>
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} isLoading={isSubmitting} type="submit">
              Create
            </Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default NewGameModal;
