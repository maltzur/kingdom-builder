/* eslint-disable react/jsx-props-no-spreading */
import {
  Box,
  ButtonGroup,
  IconButton,
  IconProps,
  useRadio,
  useRadioGroup,
  UseRadioProps,
  Wrap,
  WrapItem,
} from '@chakra-ui/react';
import { ReactElement } from 'react';
import { Control, useController } from 'react-hook-form';
import {
  BlueSettlementIcon,
  CyanSettlementIcon,
  GraySettlementIcon,
  GreenSettlementIcon,
  KhakieSettlementIcon,
  OrangeSettlementIcon,
  PinkSettlementIcon,
  PurpleSettlementIcon,
  RedSettlementIcon,
  WhiteSettlementIcon,
  YellowSettlementIcon,
} from 'src/components/settlements';
import { FormData } from 'src/layouts/HomeLayout/NewGameModal';

interface RadioCardProps extends UseRadioProps {
  label: string;
  icon: ReactElement<IconProps>;
}

const RadioCard = ({ label, icon, ...radioProps }: RadioCardProps): JSX.Element => {
  const { getInputProps, getCheckboxProps, state } = useRadio({ ...radioProps });

  const input = getInputProps();
  const checkbox = getCheckboxProps();

  return (
    <Box as="label">
      <input {...input} />
      <IconButton
        as="div"
        {...checkbox}
        aria-label={label}
        icon={icon}
        cursor="pointer"
        colorScheme={state.isChecked ? 'cyan' : 'gray'}
      />
    </Box>
  );
};

const SettlementRadio = ({ control }: {
  control: Control<FormData, object>; // eslint-disable-line @typescript-eslint/ban-types
}): JSX.Element => {
  // https://github.com/chakra-ui/chakra-ui/discussions/2385
  // https://github.com/react-hook-form/react-hook-form/discussions/4591#discussioncomment-563645
  const { field } = useController({
    control,
    name: 'color',
    rules: { required: 'Color is required' },
  });

  const { getRootProps, getRadioProps } = useRadioGroup({ ...field });
  const group = getRootProps();

  return (
    <ButtonGroup size="lg" {...group}>
      <Wrap>
        <WrapItem>
          <RadioCard
            label="Red settlement"
            icon={<RedSettlementIcon />}
            {...getRadioProps({ value: '1' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Green settlement"
            icon={<GreenSettlementIcon />}
            {...getRadioProps({ value: '2' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Blue settlement"
            icon={<BlueSettlementIcon />}
            {...getRadioProps({ value: '3' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Yellow settlement"
            icon={<YellowSettlementIcon />}
            {...getRadioProps({ value: '4' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="White settlement"
            icon={<WhiteSettlementIcon />}
            {...getRadioProps({ value: '5' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Pink settlement"
            icon={<PinkSettlementIcon />}
            {...getRadioProps({ value: '6' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Purple settlement"
            icon={<PurpleSettlementIcon />}
            {...getRadioProps({ value: '7' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Cyan settlement"
            icon={<CyanSettlementIcon />}
            {...getRadioProps({ value: '8' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Orange settlement"
            icon={<OrangeSettlementIcon />}
            {...getRadioProps({ value: '9' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Khakie settlement"
            icon={<KhakieSettlementIcon />}
            {...getRadioProps({ value: '10' })}
          />
        </WrapItem>
        <WrapItem>
          <RadioCard
            label="Gray settlement"
            icon={<GraySettlementIcon />}
            {...getRadioProps({ value: '11' })}
          />
        </WrapItem>
      </Wrap>
    </ButtonGroup>
  );
};

export default SettlementRadio;
