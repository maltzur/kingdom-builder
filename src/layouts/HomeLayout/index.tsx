import {
  Button, Center, Flex, Heading, Spacer, useDisclosure, VStack,
} from '@chakra-ui/react';
import { invoke } from '@tauri-apps/api/tauri';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import NewGameModal from './NewGameModal';

const HomeLayout = (): JSX.Element => {
  const history = useHistory();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const playNow = useCallback(
    () => {
      history.push('/play');
    },
    [history],
  );

  const newGame = useCallback(
    () => {
      invoke('new_game').then((message) => {
        console.log(message);
      });
    },
    [],
  );

  return (
    <>
      <VStack h="100vh" align="stretch" spacing="20px">
        <Center bgColor="blue.500" p="20px">
          <Heading>Kingdom Builder</Heading>
        </Center>
        <Flex direction="column" h="100%">
          <Spacer />
          <Center>
            <VStack align="stretch">
              <Button onClick={playNow}>Play Now</Button>
              <Button onClick={onOpen}>New Game</Button>
            </VStack>
          </Center>
          <Spacer />
        </Flex>
      </VStack>
      <NewGameModal isOpen={isOpen} onClose={onClose} />
    </>
  );
};

export default HomeLayout;
