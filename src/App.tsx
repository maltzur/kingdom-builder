import { ChakraProvider } from '@chakra-ui/react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import HomeLayout from 'src/layouts/HomeLayout';
import TableLayout from 'src/layouts/TableLayout';
import theme from 'src/theme';
import './index.scss';

const App = (): JSX.Element => (
  <ChakraProvider theme={theme}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <HomeLayout />
        </Route>
        <Route path="/table/:id">
          <TableLayout />
        </Route>
      </Switch>
    </BrowserRouter>
  </ChakraProvider>
);

export default App;
