import { ColorModeScript } from '@chakra-ui/react';
import ReactDOM from 'react-dom';
import App from './App';
import theme from './theme';

const MOUNT_NODE = document.getElementById('root');

ReactDOM.render(
  <>
    <ColorModeScript initialColorMode={theme.config.initialColorMode} />
    <App />
  </>,
  MOUNT_NODE,
);
