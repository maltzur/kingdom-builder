import { Icon } from '@chakra-ui/react';

const SettlementIcon = ({ fill, stroke }: {
  fill?: string | undefined;
  stroke?: string | undefined;
}) => (
  <Icon viewBox="0 0 35 35">
    <path
      id="Sans nom"
      fill={fill}
      fillOpacity="1"
      stroke="none"
      strokeWidth="1"
      strokeLinecap="round"
      strokeLinejoin="bevel"
      d="m 0.69589692,14.356239 c 0,0 10.96000008,-10.3500001 10.96000008,-10.3500001 0,0 10.78,-3.34999993 10.78,-3.34999993 0,0 10.53,11.74000003 10.53,11.74000003 0,0 -0.13,16.66 -0.13,16.66 0,0 -11.7,5.86 -11.7,5.86 0,0 -20.35000008,-4.04 -20.35000008,-4.04 0,0 -0.13,-16.52 -0.13,-16.52"
    />
    <path
      id="path827"
      fill="none"
      stroke={stroke}
      strokeWidth="0.93749785"
      strokeLinecap="butt"
      strokeLinejoin="bevel"
      strokeMiterlimit="4"
      strokeDasharray="none"
      strokeOpacity="1"
      d="m 11.655897,4.0062389 9.496069,14.0067351 -0.01607,16.903265"
    />
    <path
      id="path829"
      fill="none"
      stroke={stroke}
      strokeWidth="0.93749785"
      strokeLinecap="butt"
      strokeLinejoin="miter"
      strokeMiterlimit="4"
      strokeDasharray="none"
      strokeOpacity="1"
      d="M 21.151966,18.012974 32.965897,12.396239"
    />
    <path
      id="Sans nom-3"
      fill="none"
      fillOpacity="1"
      stroke="#000000"
      strokeWidth="1.31249702"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeMiterlimit="4"
      strokeDasharray="none"
      strokeOpacity="1"
      d="m 0.69589702,14.356239 c 0,0 10.95999998,-10.3500001 10.95999998,-10.3500001 0,0 10.78,-3.34999993 10.78,-3.34999993 0,0 10.530001,11.74000003 10.530001,11.74000003 0,0 -0.13,16.66 -0.13,16.66 0,0 -11.700001,5.860001 -11.700001,5.860001 0,0 -20.34999998,-4.040001 -20.34999998,-4.040001 0,0 -0.13,-16.52 -0.13,-16.52"
    />

  </Icon>
);

SettlementIcon.defaultProps = {
  fill: undefined,
  stroke: undefined,
};

export const RedSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#d42b2b" stroke="#a72121" />
);

export const GreenSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#5cbe31" stroke="#3f911b" />
);

export const BlueSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#5382c6" stroke="#225195" />
);

export const YellowSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#ffa500" stroke="#966100" />
);

export const WhiteSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#ffffff" stroke="#5e5c5c" />
);

export const PinkSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#e94190" stroke="#ae145d" />
);

export const PurpleSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#982fff" stroke="#50009e" />
);

export const CyanSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#72c3b1" stroke="#357d6d" />
);

export const OrangeSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#f07f16" stroke="#a3540a" />
);

export const KhakieSettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#bdd002" stroke="#7b8801" />
);

export const GraySettlementIcon = (): JSX.Element => (
  <SettlementIcon fill="#7b7b7b" stroke="#414141" />
);
