export type Resource = 'Grass'
| 'Forest'
| 'FlowerForest'
| 'Desert'
| 'Canyon'
| 'Water'
| 'Mountain'
| 'Castle'
| 'Location';

export type Location = 'Oracle'
| 'Farm'
| 'Oasis'
| 'Tower'
| 'Tavern'
| 'Barn'
| 'Harbor'
| 'Paddock';

export type Quadrant = {
  location: Location;
  flipped: boolean;
};

export type Tile = {
  settlement: number;
  resource: Resource;
  location?: Location;
};
