import { Box } from '@chakra-ui/react';
import { invoke } from '@tauri-apps/api';
import { listen } from '@tauri-apps/api/event';
import { getCurrent } from '@tauri-apps/api/window';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { Tile } from 'src/components/types';
import styled, { css } from 'styled-components';
import './styles.scss';

const Settlement = styled.div<{
  settlementId: number;
}>`
  position: absolute;
  display: block;
  width: 60%;
  height: 60%;
  left: 20%;
  top: 20%;
  background-image: url("/settlements.svg");
  background-size: 1100%;
  background-repeat: no-repeat;
  z-index: 10;
  background-position: ${(props) => props.settlementId * 10 - 10}%;
`;

const GridContent = styled.div<{
  settlementId?: number;
}>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 2;
  background-color: transparent;
  clip-path: polygon(50% 0, 100% 25%, 100% 75%, 50% 100%, 0 75%, 0 25%);
  ${({ settlementId }) => {
    if (settlementId === 1) {
      return {
        'background-color': 'rgba(167, 33, 33, 0.3)',
        'background-position': '9.09%',
      };
    }
    if (settlementId === 2) {
      return {
        'background-color': 'rbga(63, 145, 27, .3)',
        'background-position': '18.18%',
      };
    }
    if (settlementId === 3) {
      return {
        'background-color': 'rgba(34, 81, 149, .3)',
        'background-position': '27.27%',
      };
    }
    if (settlementId === 4) {
      return {
        'background-color': 'rgba(150, 97, 0, .3)',
        'background-position': '36.36%',
      };
    }
    if (settlementId === 5) {
      return {
        'background-color': 'hlsa(0, 0%, 100%, .3)',
        'background-position': '45.45%',
      };
    }
    if (settlementId === 6) {
      return {
        'background-color': 'rgba(174, 20, 93, .3)',
        'background-position': '54.54%',
      };
    }
    if (settlementId === 7) {
      return {
        'background-color': 'rgba(80, 0, 158, .3)',
        'background-position': '63.63%',
      };
    }
    if (settlementId === 8) {
      return {
        'background-color': 'rgba(53, 125, 109, .3)',
        'background-position': '72.72%',
      };
    }
    if (settlementId === 9) {
      return {
        'background-color': 'rgba(163, 84, 10, .3)',
        'background-position': '81.81%',
      };
    }
    if (settlementId === 10) {
      return {
        'background-position': 'rgba(',
      };
    }
    return undefined;
  }}
`;

enum MoveAction {
  Bulk,
  Add,
  Remove,
  Move,
}

const GridTile = ({ tableId, x, y }: {
  tableId: number;
  x: number;
  y: number;
}): JSX.Element => {
  const [tile, setTile] = useState<Tile>();

  useEffect(() => {
    invoke<Tile>('get_tile', { tableId, x, y }).then(setTile);
  }, [tableId, x, y]);

  useEffect(() => {
    listen(`x${x}y${y}`, ({ event, payload }) => {
      // const { action } = payload;
    });
  }, [x, y]);

  const onClick = () => {
    const currentWindow = getCurrent();
    const payload = {
      action: 'Remove',
      payload: { player: 1, x, y },
    };
    currentWindow.emit('move', JSON.stringify(payload));
  };

  const contentClassNames = classNames({
    'hex-grid__content': true,
    'player-1': true,
  });

  const settlementClassNames = classNames({
    settlement: true,
    // [`player-${tile?.settlement}`]: tile?.settlement,
    'player-1': true,
  });

  const xStart = x * 3 + 1;
  const yStart = y * 2 + (x % 2) + 1;
  const gridArea = `${xStart} / ${yStart} / span 4 / span 2`;

  return (
    <li className="hex-grid__item" style={{ gridArea }}>
      <Box onClick={onClick} className={contentClassNames}>
        <Box className={settlementClassNames} />
      </Box>
    </li>
  );
};

export default GridTile;
