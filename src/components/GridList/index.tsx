import { Skeleton } from '@chakra-ui/react';
import { invoke } from '@tauri-apps/api';
import classNames from 'classnames';
import { useState, useEffect } from 'react';
import GridTile from 'src/components/GridTile';
import { Quadrant } from 'src/components/types';
import './styles.scss';

const quadrantClassNames = (quadrant: Quadrant): string => classNames({
  'hex-grid__quadrant': true,
  'quadrant-0': quadrant.location === 'Barn',
  'quadrant-1': quadrant.location === 'Farm',
  'quadrant-2': quadrant.location === 'Oracle',
  'quadrant-3': quadrant.location === 'Harbor',
  'quadrant-4': quadrant.location === 'Tower',
  'quadrant-5': quadrant.location === 'Oasis',
  'quadrant-6': quadrant.location === 'Paddock',
  'quadrant-7': quadrant.location === 'Tavern',
  flipped: quadrant.flipped,
});

const GridList = ({ tableId }: {
  tableId: number;
}): JSX.Element => {
  const [quadrants, setQuadrants] = useState<Quadrant[]>();

  useEffect(() => {
    invoke<Quadrant[]>('get_quadrants', { tableId }).then((message) => {
      setQuadrants(message);
    });
  }, [tableId]);

  return (
    <ul className="hex-grid__container">
      {/* <Skeleton isLoaded={!quadrants}> */}
      <li id="quadrant-top-left" className={quadrants && quadrantClassNames(quadrants[0])} />
      <li id="quadrant-top-right" className={quadrants && quadrantClassNames(quadrants[1])} />
      <li id="quadrant-bottom-left" className={quadrants && quadrantClassNames(quadrants[2])} />
      <li id="quadrant-bottom-right" className={quadrants && quadrantClassNames(quadrants[3])} />
      {Array.from({ length: 20 * 20 }, (_, index) => (
        <GridTile tableId={tableId} x={Math.floor(index / 20)} y={index % 20} key={index} />
      ))}
      {/* </Skeleton> */}
    </ul>
  );
};

export default GridList;
