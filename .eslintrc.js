module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'airbnb',
    'airbnb-typescript',
    'airbnb/hooks',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    // 'plugin:import/recommended',
    // 'plugin:import/typescript',
  ],
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2020,
  },
  rules: {
    'radix': ['error', 'as-needed'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'react/react-in-jsx-scope': 'off',
    'react/jsx-filename-extension': [2, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'import/order': [
      'error',
      {
        'alphabetize': { 'order': 'asc' },
      }
    ]
    // 'sort-imports': [
    //   'error',
    //   {
    //     'ignoreCase': false,
    //     'ignoreDeclarationSort': false,
    //     'ignoreMemberSort': false,
    //     'memberSyntaxSortOrder': ['none', 'all', 'multiple', 'single'],
    //     'allowSeparatedGroups': true,
    //   }
    // ],
    // 'import/extensions': [
    //   'error',
    //   'ignorePackages',
    //   {
    //     'js': 'never',
    //     'jsx': 'never',
    //     'ts': 'never',
    //     'tsx': 'never',
    //   }
    // ]
  },
  settings: {
    // 'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
    // 'import/parsers': {
    //   '@typescript-eslint/parser': ['.ts', '.tsx'],
    // },
    // 'import/resolver': {
    //   'typescript': { 'alwaysTryTypes': true },
    //   'node': {
    //     'extensions': ['.js', '.jsx', '.ts', '.tsx'],
    //     'moduleDirectory': ['node_modules',],
    //   }
    // }
  }
};
