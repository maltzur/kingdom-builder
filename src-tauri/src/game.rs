use byteorder::BigEndian;
use serde::{Deserialize, Serialize};
use zerocopy::{byteorder::U64, AsBytes, FromBytes, LayoutVerified, Unaligned};

use crate::result::Result;

pub trait Sledder {
    type Key: FromBytes + AsBytes;
    type Value: FromBytes + AsBytes;

    fn to_sled(&self, db: &sled::Db) -> Result<()>;

    fn from_sled(db: &sled::Db, key: Self::Key) -> Result<Self>
    where
        Self: Sized;
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Table {
    id: u64,
    players: [u8; 5],
    board: Board,
}

#[derive(FromBytes, AsBytes, Unaligned)]
#[repr(C)]
pub struct TableKey {
    id: U64<BigEndian>,
}

#[derive(FromBytes, AsBytes, Unaligned)]
#[repr(C)]
pub struct TableValue {
    players: [u8; 5],
}

impl Table {
    pub fn new(db: &sled::Db) -> Result<Self> {
        let id = db.generate_id()?;
        let table = Table {
            id,
            players: [0; 5],
        };
        table.to_sled(db)?;
        Ok(table)
    }

    // pub fn from_db(db: &sled::Db, id: U64<BigEndian>) -> Result<Self> {
    //     let tree = db.open_tree(b"tables")?;
    //     Self::from_tree(&tree, id)
    // }

    // pub fn from_tree(tree: &sled::Tree, id: U64<BigEndian>) -> Result<Self> {
    //     let table = tree.get(id.as_bytes())?;
    //     Ok(Self { id })
    // }
}

impl Sledder for Table {
    type Key = TableKey;
    type Value = TableValue;

    fn to_sled(&self, db: &sled::Db) -> Result<()> {
        let key = TableKey { id: self.id.into() };
        let value = TableValue {
            players: self.players,
        };

        db.insert(key.as_bytes(), value.as_bytes())?;

        Ok(())
    }

    fn from_sled(db: &sled::Db, key: Self::Key) -> Result<Self>
    where
        Self: Sized,
    {
        let tree = db.open_tree(b"tables")?;
        let bytes = tree.get(key.as_bytes())?.unwrap();
        let layout: LayoutVerified<&[u8], TableValue> =
            LayoutVerified::new(&*bytes).expect("bytes do not fit schema");
        let value: &TableValue = layout.into_ref();

        Ok(Self {
            id: key.id.into(),
            players: value.players,
        })
    }
}

struct Player {
    name: String,
}

// impl Board {
//   fn from_tree(tree: &sled::Tree) -> Self {
//     let start = TileKey { x:  }
//     tree.range(start..end);
//   }
// }

#[derive(Clone, Copy, PartialEq, Eq, Debug, Serialize, Deserialize)]
#[repr(u8)]
pub enum Location {
    None = 0,
    Oracle = 1,
    Farm = 2,
    Oasis = 3,
    Tower = 4,
    Tavern = 5,
    Barn = 6,
    Harbor = 7,
    Paddock = 8,
}

impl Location {
    pub(crate) const fn into(self) -> u8 {
        self as u8
    }
}

impl From<u8> for Location {
    fn from(byte: u8) -> Self {
        use Location::*;

        match byte {
            0 => None,
            1 => Oracle,
            2 => Farm,
            3 => Oasis,
            4 => Tower,
            5 => Tavern,
            6 => Barn,
            7 => Harbor,
            8 => Paddock,
            other => {
                eprintln!("uncountered unexpected tile location variant {}", other);
                None
            }
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Serialize, Deserialize)]
struct Tile {
    x: u8,
    y: u8,
    player: u8,
    location: Location,
}

#[derive(FromBytes, AsBytes, Unaligned)]
#[repr(C)]
struct TileKey {
    x: u8,
    y: u8,
}

#[derive(FromBytes, AsBytes, Unaligned)]
#[repr(C)]
struct TileValue {
    player: u8,
    location: u8,
}

impl Sledder for Tile {
    type Key = TileKey;
    type Value = TileValue;

    fn to_sled(&self, db: &sled::Db) -> Result<()> {
        let key = TileKey {
            x: self.x,
            y: self.y,
        };
        let value = TileValue {
            player: self.player,
            location: self.location.into(),
        };

        db.insert(key.as_bytes(), value.as_bytes())?;

        Ok(())
    }

    fn from_sled(db: &sled::Db, key: Self::Key) -> Result<Self>
    where
        Self: Sized,
    {
        let bytes = db.get(key.as_bytes())?.unwrap();
        let layout: LayoutVerified<&[u8], TileValue> =
            LayoutVerified::new(&*bytes).expect("bytes do not fit schema");
        let value: &TileValue = layout.into_ref();
        Ok(Self {
            x: key.x,
            y: key.y,
            player: value.player,
            location: Location::from(value.location),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_create_table() {
        let db = sled::open("test_db").unwrap();
    }
}
