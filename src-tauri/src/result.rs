use std::{
    error::Error as StdError,
    fmt::{self, Display},
    io,
};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    /// A read or write error has happened when interacting with the file
    /// system.
    Io(io::Error),
    /// An error has happened when interacting with the sled database.
    Sled(sled::Error),
    /// An error has happened when invoking a tauri command or event.
    Tauri(tauri::InvokeError),
}

impl Clone for Error {
    fn clone(&self) -> Self {
        use self::Error::*;

        match self {
            Io(e) => Io(io::Error::new(e.kind(), format!("{:?}", e))),
            Sled(e) => Sled(e.clone()),
            Tauri(e) => Tauri(e.clone()),
        }
    }
}

impl Eq for Error {}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        use self::Error::*;

        match *self {
            Io(_) => false,
            Sled(ref lhs) => {
                if let Sled(ref rhs) = *other {
                    lhs == rhs
                } else {
                    false
                }
            },
        }
    }
}

impl StdError for Error {}

impl From<io::Error> for Error {
    #[inline]
    fn from(io_error: io::Error) -> Self {
        Error::Io(io_error)
    }
}

impl From<Error> for io::Error {
    fn from(error: Error) -> Self {
        use self::Error::*;
        use std::io::ErrorKind;

        match error {
            Io(e) => e,
            Sled(e) => Self::from(e),
            // CollectionNotFound(name) =>
            //     io::Error::new(
            //     ErrorKind::NotFound,
            //     format!("collection not found: {:?}", name),
            // ),
        }
    }
}

impl From<sled::Error> for Error {
    #[inline]
    fn from(sled_error: sled::Error) -> Self {
        Error::Sled(sled_error)
    }
}

impl From<tauri::InvokeError> for Error {
    #[inline]
    fn from(tauri_error: tauri::InvokeError) -> Self {

    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::result::Result<(), fmt::Error> {
        use self::Error::*;

        match *self {
            Io(ref e) => write!(f, "IO error: {}", e),
            Sled(ref e) => write!(f, "{}", e),
        }
    }
}
