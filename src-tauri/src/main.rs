#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
#![feature(array_chunks, int_roundings)]

mod cmd;
mod quadrants;
// mod result;

use std::{error::Error, sync::Arc};

use cmd::{DatabaseState, get_quadrants, get_tile, new_game};

fn main() -> Result<(), Box<dyn Error>> {
    let db = Arc::new(sled::open("kingdombuilder")?);

    tauri::Builder::default()
        .manage(DatabaseState { db: db.clone() })
        .invoke_handler(tauri::generate_handler![new_game, get_quadrants, get_tile])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

    Ok(())
}
