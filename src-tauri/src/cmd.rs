use std::{collections::HashMap, error::Error, ops::{Add, Deref}, sync::Arc};

use byteorder::{ByteOrder, NativeEndian};
use rand::{Rng, prelude::{IteratorRandom, ThreadRng}, seq::SliceRandom};
use rkyv::{
    Archive,
    Archived,
    Deserialize,
    Infallible,
    Serialize,
    archived_root,
    ser::{Serializer, serializers::AllocSerializer},
};
use sled::transaction::TransactionalTree;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use zerocopy::AsBytes;

use crate::{
    quadrants::{
        BARN_TILES,
        FARM_TILES,
        HARBOR_TILES,
        OASIS_TILES,
        ORACLE_TILES,
        PADDOCK_TILES,
        TAVERN_TILES,
        TOWER_TILES,
    },
};

pub trait Sledder<const N: usize>
where
    Self: Sized + Serialize<AllocSerializer<N>>,
    Archived<Self>: Deserialize<Self, Infallible>,
{
    fn load(tree: &sled::Tree, key: Key) -> Self {
        let value_bytes = tree.get(key).unwrap().unwrap();
        let archived = unsafe { archived_root::<Self>(&value_bytes) };
        archived.deserialize(&mut Infallible).unwrap()
    }

    fn save(self, tree: &sled::Tree, key: Key) -> Self {
        let value_bytes = Self::serialize_self(&self);
        tree.insert(key, value_bytes).unwrap();
        self
    }

    fn save_transaction(self, transaction: &TransactionalTree, key: Key) -> Self {
        let value_bytes = Self::serialize_self(&self);
        transaction.insert(key, value_bytes).unwrap();
        self
    }

    fn serialize_self<'a>(&self) -> Vec<u8> {
        let mut serializer = AllocSerializer::<N>::default();
        serializer.serialize_value(self).unwrap();
        let buf = serializer.into_serializer().into_inner();
        (&buf[..]).to_vec()
    }
}

pub struct DatabaseState {
    pub db: Arc<sled::Db>,
}

#[derive(Debug, Archive, Serialize, Deserialize)]
pub struct Key(Vec<u8>);

impl Key {
    fn new(parts: Vec<u8>) -> Self {
        Self(parts)
    }

    fn add(mut self, part: &[u8]) -> Self {
        self.0.extend_from_slice(part);
        self
    }

    fn scan_table(self) -> Self {
        self.add(b":table:")
    }

    fn table(self, table_id: &u64) -> Self {
        self.scan_table().add(table_id.as_bytes())
    }

    fn scan_quadrant(self) -> Self {
        self.add(b":quadrant:")
    }

    fn quadrant(self, quadrant_id: &u8) -> Self {
        self.scan_quadrant().add(quadrant_id.as_bytes())
    }

    fn scan_x(self) -> Self {
        self.add(b":x:")
    }

    fn x(self, x: &u8) -> Self {
        self.scan_x().add(x.as_bytes())
    }

    fn scan_y(self) -> Self {
        self.add(b":y:")
    }

    fn y(self, y: &u8) -> Self {
        self.scan_y().add(y.as_bytes())
    }

    fn scan_player(self) -> Self {
        self.add(b":player:")
    }

    fn player(self, player_id: &u8) -> Self {
        self.scan_player().add(player_id.as_bytes())
    }

    fn scan_user_tables(self, user_id: &u64) -> Self {
        self.add(b":usertables:").add(user_id.as_bytes())
    }

    fn user_tables(self, user_id: &u64, table_id: &u64) -> Self {
        self.scan_user_tables(user_id).add(table_id.as_bytes())
    }
}

impl Default for Key {
    fn default() -> Self {
        Self(Vec::with_capacity(32))
    }
}

impl From<&[u8]> for Key {
    fn from(slice: &[u8]) -> Self {
        Self::new(slice.to_vec())
    }
}

impl From<Key> for sled::IVec {
    fn from(key: Key) -> Self {
        sled::IVec::from(key.0)
    }
}

impl AsRef<[u8]> for Key {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl Deref for Key {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Archive, Serialize, Deserialize, serde::Serialize, serde::Deserialize)]
pub struct Quadrant {
    location: Location,
    flipped: bool,
}

impl Quadrant {
    fn new(location: Location) -> Self {
        Self { location, flipped: false }
    }

    /// Sets the [`Quadrant`] to a random `flipped` state with `p` probability.
    fn rand_flip(&mut self, rng: &mut ThreadRng, p: f64) -> &mut Self {
        self.flipped = rng.gen_bool(p);
        self
    }
}

impl Sledder<32> for Quadrant {}

#[derive(Clone, Copy, PartialEq, Debug, Archive, Serialize, Deserialize, serde::Serialize, serde::Deserialize)]
pub struct Tile {
    #[serde(flatten)]
    resource: Resource,
    settlement: u8,
}

impl Tile {
    fn new(resource: Resource, settlement: u8) -> Self {
        Self { resource, settlement }
    }
}

impl Sledder<32> for Tile {}

#[derive(Clone, Copy, PartialEq, Debug, EnumIter, Archive, Serialize, Deserialize, serde::Serialize, serde::Deserialize)]
#[repr(u8)]
pub enum Location {
    Oracle,
    Farm,
    Oasis,
    Tower,
    Tavern,
    Barn,
    Harbor,
    Paddock,
}

impl Location {
    pub(crate) const fn into(self) -> u8 {
        self as u8
    }
}

impl From<u8> for Location {
    fn from(byte: u8) -> Self {
        use Location::*;

        match byte {
            0 => Oracle,
            1 => Farm,
            2 => Oasis,
            3 => Tower,
            4 => Tavern,
            5 => Barn,
            6 => Harbor,
            7 => Paddock,
            other => {
                panic!("encountered unexpected tile qudrant variant {}", other);
            }
        }
    }
}

impl From<Location> for sled::IVec {
    fn from(location: Location) -> Self {
        sled::IVec::from(&[location.into()])
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Archive, Serialize, Deserialize, serde::Serialize, serde::Deserialize)]
#[repr(u8)]
#[serde(tag = "resource", content = "location")]
pub enum Resource {
    Grass,
    Forest,
    FlowerForest,
    Desert,
    Canyon,
    Water,
    Mountain,
    Castle,
    Location(Location),
}

/// Generate and save a new table id using [`sled::Db::generate_id`].
fn generate_table_id(tx_db: &TransactionalTree) -> u64 {
    // We store all the table ids in a single value.
    // Is this the best way, or is there something better?

    let table_id = tx_db.generate_id().unwrap();

    let key = Key::default()
        .user_tables(&1, &table_id);  // Since we have no notion of users, we just use user=1
    tx_db.insert(key, vec![1]).unwrap();

    table_id
}

/// Generate random quadrants and save them in the table tree.
fn generate_quadrants(tx_db: &TransactionalTree, table_id: &u64) -> Result<(), sled::Error> {
    let mut rng = rand::thread_rng();
    let location_choices: Vec<Location> = Location::iter().collect();
    let mut locations: Vec<Location> = location_choices.choose_multiple(&mut rng, 4).cloned().collect();
    locations.shuffle(&mut rng);

    let mut new = |quadrant_id: u8| {
        let location = locations[(quadrant_id - 1) as usize];

        let key = Key::default().table(table_id).quadrant(&quadrant_id);
        let quadrant = Quadrant::new(location)
            .rand_flip(&mut rng, 0.25)
            .save_transaction(tx_db, key);

        let offset = match quadrant_id {
            1 => (0, 0),
            2 => (10, 0),
            3 => (0, 10),
            4 => (10, 10),
            _ => panic!("invalid quadrant"),
        };
        generate_tiles(tx_db, table_id, &quadrant, offset);
    };

    for i in 1..=4 {
        new(i);
    }

    Ok(())
}

fn generate_tiles(tx_db: &TransactionalTree, table_id: &u64, quadrant: &Quadrant, offset: (u8, u8)) {
    let location = Location::from(quadrant.location);
    let tiles = match location {
        Location::Barn => BARN_TILES,
        Location::Farm => FARM_TILES,
        Location::Harbor => HARBOR_TILES,
        Location::Oasis => OASIS_TILES,
        Location::Oracle => ORACLE_TILES,
        Location::Paddock => PADDOCK_TILES,
        Location::Tavern => TAVERN_TILES,
        Location::Tower => TOWER_TILES,
    };
    let chunks = tiles.array_chunks::<10>();
    for (x, col) in chunks.enumerate() {
        for (y, value) in col.iter().enumerate() {
            let x = x as u8 + offset.0;
            let y = y as u8 + offset.1;
            let key = Key::default().table(table_id).x(&x).y(&y);

            let resource = match *value {
                1 => Resource::Grass,
                2 => Resource::Forest,
                3 => Resource::FlowerForest,
                4 => Resource::Desert,
                5 => Resource::Canyon,
                6 => Resource::Water,
                7 => Resource::Mountain,
                8 => Resource::Castle,
                9 => Resource::Location(quadrant.location),
                _ => panic!("Invalid resource"),
            };
            let tile = Tile::new(resource, 0);
            tile.save_transaction(&tx_db, key);
        }
    }
}

/// Generate players and the player order.
fn generate_players(tx_db: &TransactionalTree, table_id: &u64, num_players: u8, selected_color: u8) {
    let mut rng = rand::thread_rng();
    // Remove the selected color as a choice for opponents
    let color_choices = (1..=11).filter(|value| *value != selected_color);
    // Get random colors for the opponents
    let mut players: Vec<u8> = color_choices.choose_multiple(&mut rng, (num_players - 1).into());
    players.push(selected_color);
    // Since `rand` does not guarentee random order, we need to shuffle
    players.shuffle(&mut rng);

    for i in players {
        let key = Key::default()
            .table(table_id)
            .player(&i);
        tx_db.insert(key, &[1]).unwrap();
    }
}

#[tauri::command]
pub fn new_game(
    state: tauri::State<DatabaseState>,
    num_players: u8,
    color: u8,
) -> Result<u64, String> {
    let db = &state.db;

    let table_id: Result<u64, sled::transaction::TransactionError> = db.transaction(|tx_db| {
        let table_id = generate_table_id(tx_db);
        generate_quadrants(tx_db, &table_id)?;
        generate_players(tx_db, &table_id, num_players, color);

        Ok(table_id)
    });

    Ok(table_id.unwrap())
}

#[tauri::command]
pub fn get_quadrants(
    state: tauri::State<DatabaseState>,
    table_id: u64,
) -> Result<Vec<Quadrant>, String> {
    let db = &state.db;

    let get_quadrant = |i: u8| {
        let key = Key::default().table(&table_id).quadrant(&i);
        Quadrant::load(db, key)
    };

    Ok((1..=4).map(get_quadrant).collect())
}

#[tauri::command]
pub fn get_tile(
    state: tauri::State<DatabaseState>,
    table_id: u64,
    x: u8,
    y: u8,
) -> Result<Tile, String> {
    let db = &state.db;

    let key = Key::default().table(&table_id).x(&x).y(&y);
    Ok(Tile::load(db, key))
}

#[cfg(test)]
mod tests {
    use rand::{distributions::Alphanumeric, Rng};
    use tempfile::TempDir;
    use zerocopy::AsBytes;
    use super::*;

    fn get_db() -> sled::Db {
        let temp_dir = TempDir::new().unwrap();

        // Create a new underlying Sled DB with the test data.
        let filename: String = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(7)
            .map(char::from)
            .collect();

        sled::open(&temp_dir.path().join(filename)).unwrap()
    }

    #[test]
    fn all_tree_ids_are_retrieved() {
        let db = get_db();

        let id1 = db.generate_id().unwrap();
        let id2 = db.generate_id().unwrap();
        let id3 = db.generate_id().unwrap();
        let id4 = db.generate_id().unwrap();
        let id5 = db.generate_id().unwrap();
        let id6 = db.generate_id().unwrap();
        let id7 = db.generate_id().unwrap();
        let ids = vec![
            id1, id2, id3, id4, id5, id6, id7,
        ];

        db.insert(b"tables", ids.as_bytes()).unwrap();

        let tables = db.get(b"tables").unwrap().unwrap();
        assert_eq!(tables, sled::IVec::from(ids.as_bytes()));
    }
}
