from pathlib import Path

grass = 1
forest = 2
flower = 3
desert = 4
canyon = 5
water = 6
mountain = 7
castle = 8
location = 9

code = ''

in_dir = Path('quadrants')
for quadrant_file in in_dir.iterdir():
    with open(quadrant_file) as f:
        contents = f.read()
    for i, char in enumerate('GRFDYWMCL'):
        contents = contents.replace(char, str(i + 1))
    contents = contents.splitlines()

    file_name = quadrant_file.stem.upper()
    code += f"pub const {file_name}_TILES: [u8; 100] = [\n"

    for content in contents:
        code += f"    "
        for char in content:
            code += f'{char}, '
        code += '\n'

    code += '];\n'

with open('src/quadrants.rs', 'w') as f:
    f.write(code)
