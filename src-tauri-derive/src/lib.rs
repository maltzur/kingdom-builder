use proc_macro2::TokenStream;
use quote::{ToTokens, format_ident, quote, quote_spanned};
use syn::{Data, DeriveInput, Fields, GenericParam, Generics, LitByteStr, parse_macro_input, parse_quote, spanned::Spanned};

#[proc_macro_derive(ByteKey)]
pub fn derive_byte_key(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree
    let input = parse_macro_input!(input as DeriveInput);

    // Used in the quasi-quotation below as `#name`
    let name = input.ident;

    // Add a bound `T: ByteKey` to every type parameter T
    let generics = add_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    // Generate an expression to get the byte key
    let expr = byte_key_expr(&input.data);

    let expanded = quote! {
        // The generated impl
        impl #impl_generics kingdombuilder::ByteKey for #name #ty_generics #where_clause {
            fn key(&self) -> &[u8] {
                #expr
            }
        }
    };

    // Hand the output tokens back to the compiler
    expanded.into()
}

// Add a bound `T: ByteKey` to every type parameter T
fn add_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(kingdombuilder::ByteKey));
        }
    }
    generics
}

// Generate an expression to get the byte key
fn byte_key_expr(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let recurse = fields.named.iter().map(|f| {
                        let name_ident = &f.ident;
                        let name_str = name_ident.as_ref().unwrap().to_string();
                        let name_bytes = name_str.as_bytes();
                        let name_token = LitByteStr::new(&name_bytes, f.span());
                        quote_spanned! {f.span()=>
                            #name_token,
                        }
                    });
                    quote! {

                    }
                },
                // TODO: error
                _ => unimplemented!(),
            }
        },
        _ => unimplemented!(),
    }
}
